'use strict'

const API = 'https://api.ipify.org/?format=json';
const ipAPI = 'http://ip-api.com/';
const btn = document.querySelector('.btn');
const root = document.querySelector('.root');


function sendRequest(url) {
    return fetch(url)
}

btn.addEventListener('click', () => {
    getIP();
    if (!!root.innerHTML){
        root.innerHTML = ''
    }
})

async function getIP(url) {
    const IP = await sendRequest(API)
        .then((response) => response.json())
        .then((data) => {
            const {ip} = data;
            getAddress(url, ip)
        })
}

async function getAddress(url, ip) {
    const address = await sendRequest(`${ipAPI}json/${ip}`)
        .then((response) => response.json())
        .then((result) => {
            const {country, region, city, regionName} = result;
            root.insertAdjacentHTML('afterbegin', `
                        <h3>Country: ${country}</h3>
                        <h3>Region: ${region}</h3>
                        <h3>City: ${city}</h3>
                        <h3>Region name: ${regionName}</h3>                
                        `);
        })
}